"""CLI to import the input CSV file into SQL Server tables
"""
import argparse
import csv
from datetime import datetime
from config import Config, Container
from model import User, Plan, Subscription

def import_file(csv_file):
    with open(csv_file) as f:
        reader = csv.reader(f, delimiter=',')
        next(reader) # skip the header row

        db = Container.db()
        db.connect()

        for record in reader:

            userid = record[0]
            appt_date = datetime.strptime(record[1], '%b %d, %Y')
            plan = record[2]
            end_date = None
            if record[3]:
                end_date = datetime.strptime(record[3], '%b %d, %Y')

            u = User.get_or_create(userid)
            p = Plan.get_or_create(plan)
            s = Subscription.add(u, p, appt_date, end_date)
    
        db.disconnect()

def main():
    Config.config.init_resources()
    Config.config.from_ini("config.ini")

    parser = argparse.ArgumentParser(description='Import user records')
    parser.add_argument("csv", help='CSV file to process')
    args = parser.parse_args()

    import_file(args.csv)

if __name__ == '__main__':
    main()