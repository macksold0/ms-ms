"""DB interface classes.=
"""
from config import Config, Container

class User:
    def __init__(self, uid):
        self.userid = uid

    @classmethod
    def get_or_create(cls, userid):
        if not userid:
            return None

        user = User.get(userid)
        if not user:
            user = User.create(userid)

        return user

    @classmethod
    def get(cls, userid):
        if not userid:
            return None

        db = Container.db()
        record = db.unique_result(
            """
            SELECT ID
            FROM Users
            WHERE ID = %s""", userid)
        if record:
            return User(record['ID'])
        else:
            return None

    @classmethod
    def create(cls, userid):
        if not userid:
            return None

        db = Container.db()

        count = db.execute_update("""
        INSERT INTO Users(ID)
        VALUES(%s)""", userid)
        if not (count == 1):
            raise Exception("Failed to create user record %s" % userid)

        return User(userid)
            
    @classmethod
    def info(cls, userid):
        if not userid:
            return None

        db = Container.db()
        config = Config.config()

        record = db.unique_result(
        f"DECLARE @GivenTaskDate datetime = '{config['main']['giventaskdate']}'" + 
        """
        DECLARE @ApptDateFmt nvarchar(20) = 'MMM d, yyyy'
        DECLARE @BillingDateFmt nvarchar(20) = 'M/d/yyyy'

        SELECT u.ID AS 'UserID',
            (SELECT TOP 1 p.Size
                FROM Plans p
                JOIN Subscriptions js ON js.PlanID = p.ID AND js.UserID = u.ID
                WHERE js.EndDate IS NULL
                ORDER BY js.AppointmentDate DESC
            ) AS 'Current plan',
            FORMAT(s.AppointmentDate, @ApptDateFmt) AS 'Subscription Start Date',
            CASE WHEN s.EndDate IS NULL
                THEN
                    CASE WHEN (DATEFROMPARTS(YEAR(@GivenTaskDate), MONTH(@GivenTaskDate), DAY(s.AppointmentDate))) > @GivenTaskDate
                        THEN FORMAT(DATEFROMPARTS(YEAR(@GivenTaskDate), MONTH(@GivenTaskDate), DAY(s.AppointmentDate)), @BillingDateFmt)
                        ELSE FORMAT(DATEADD(month, 1, DATEFROMPARTS(YEAR(@GivenTaskDate), MONTH(@GivenTaskDate), DAY(s.AppointmentDate))), @BillingDateFmt)
                    END
                ELSE
                    NULL
                END AS 'Next Billing Date',
            CASE WHEN s.EndDate IS NULL THEN 'Yes' ELSE 'No' END AS 'Is Active'

        FROM Users u
        JOIN Subscriptions s ON s.UserID = u.ID
        JOIN
        (
            SELECT ins.UserID, MIN(ins.AppointmentDate) AS AppointmentDate
            FROM Subscriptions ins
            WHERE ins.EndDate IS NULL
            GROUP BY ins.UserID

            UNION

            SELECT ins.UserID, MIN(ins.AppointmentDate) AS AppointmentDate
            FROM Subscriptions ins
            GROUP BY ins.UserID 
            HAVING COUNT(*) = 1
        ) AS x ON x.UserID = u.ID AND x.AppointmentDate = s.AppointmentDate
        WHERE u.ID = %s""", userid)

        if not (record):
            raise Exception("Failed to get user info for %s" % userid)

        # Replace None values with ''
        record = {k: '' if not v else v for k,v in record.items()}        

        return record

class Plan:
    def __init__(self, id, size):
        self.id = id
        self.size = size

    @classmethod
    def get_or_create(cls, size):
        if not size:
            return None

        plan = Plan.get_by_size(size)
        if not plan:
            id = Plan.create(size)
            plan = Plan.get(id)

        return plan

    @classmethod
    def get_by_size(cls, size):
        if not size:
            return None

        db = Container.db()
        record = db.unique_result(
            """
            SELECT ID, Size
            FROM Plans
            WHERE Size = %s""", size)
        if record:
            return Plan(record['ID'], record['Size'])
        else:
            return None

    @classmethod
    def get(cls, id):
        if not id:
            return None

        db = Container.db()
        record = db.unique_result(
            """
            SELECT ID, Size
            FROM Plans
            WHERE ID = %s""", id)
        if record:
            return Plan(record['ID'], record['Size'])
        else:
            return None

    @classmethod
    def create(cls, size):
        if not size:
            return None

        db = Container.db()

        count = db.execute_update("""
        INSERT INTO Plans(Size)
        VALUES(%s)""", size)
        if not (count == 1):
            raise Exception("Failed to create plan record %s" % size)
        
        return db.lastrowid()

class Subscription:
    @classmethod
    def add(cls, user, plan, appt_date, end_date):
        if not user or not plan or not appt_date:
            return None

        db = Container.db()

        count = db.execute_update("""
        INSERT INTO Subscriptions(UserID, AppointmentDate, EndDate, PlanID)
        VALUES(%s, %s, %s, %s)""", (user.userid, appt_date, end_date, plan.id))

        if not (count == 1):
            raise Exception("Failed to create subscription record (%s, %s, %s, %s)" % 
                (user.userid, appt_date, end_date, plan.id))
        
        return db.lastrowid()
