# README #

Subscription user API endpoint

### Dependencies ###
- Python 3
- SQL Server


### Configuration ###
Create a Python virtual environment and install dependencies listed in requirements.txt

DB table definitions in `schema.sql`

App configuration is in `config.ini` (`test.ini` for unit tests)

### Running ###
Activate your virtual environment

#### Injest the input CSV into the DB

`python injest.py <csv file>`

#### Run tests

`python tests.py`

#### Start API endpoint

`python server.py`

#### Access API endpoing at 

`http://localhost:8888/user/<userid>`
