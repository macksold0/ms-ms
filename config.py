"""Dependency injection setup
"""
from dependency_injector import containers, providers
from persister import DB

class Config(containers.DeclarativeContainer):
    config = providers.Configuration()

class Container(containers.DeclarativeContainer):
    db = providers.Singleton(DB, dbconfig=Config.config.database)
