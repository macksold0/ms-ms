"""DB connection/statement helper
"""
import pymssql

class DB:
    def __init__(self, dbconfig):
        self._conn = None
        self._cursor = None

        self._host = dbconfig['host']
        self._uid = dbconfig['uid']
        self._pwd = dbconfig['pwd']
        self._db = dbconfig['db']

        if not (self._host and self._uid and self._pwd and self._db):
            raise ValueError("No valid db config")
        
    def unique_result(self, sql, params):
        """Returns one result or None"""
        self._cursor.execute(sql, params)
        one = self._cursor.fetchone()
        if self._cursor.fetchone():
            raise ValueError("Expected 1 result but got > 1", sql)

        return one
    
    def execute(self, sql, params):
        """Returns the result set as a list of dictionaries"""
        self._cursor.execute(sql, params)
        results = []
        for row in self._cursor:
            results.append(row)

        return results

    def execute_update(self, sql, params):
        """Executes a UPDATE/INSERT/DELETE statement and returns
        the number of affected records"""
        self._cursor.execute(sql, params)
        self._conn.commit()

        return self._cursor.rowcount

    def lastrowid(self):
        return self._cursor.lastrowid

    def connect(self):
        self._conn = pymssql.connect(
            self._host, 
            self._uid,
            self._pwd,
            self._db)
        self._cursor = self._conn.cursor(as_dict=True)
    
    def disconnect(self):
        if self._conn:
            self._conn.close()
            self._conn = None
            self._cursor = None

    def __enter__(self):
        self.connect()

    def __exit__(self):
        self.disconnect()