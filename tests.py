import unittest
import csv
from config import Config, Container
from model import User

class MakeSpaceTest(unittest.TestCase):

    def test_info(self):
        """Verify results for each user
        """
        csv_file = "Output CSV file as of 9 21 2020 0574158c064942f6b1521620d7a5a512.csv"
        with open(csv_file) as f:
            reader = csv.reader(f, delimiter=',')
            next(reader) # skip the header row

            for record in reader:
                userid = record[0]
                user_info = User.info(userid)

                print(userid)

                self.assertEqual(user_info['Current plan'], record[1], userid)
                self.assertEqual(user_info['Subscription Start Date'], record[2], userid)
                self.assertEqual(user_info['Next Billing Date'], record[3], userid)
                self.assertEqual(user_info['Is Active'], record[4], userid)

if __name__ == '__main__':
    Config.config.init_resources()
    Config.config.from_ini("test.ini")

    db = Container.db()
    db.connect()

    unittest.main()

    db.disconnect()