"""HTTPD endpoint for the user api
"""
from bottle import run, route, response
import json
from config import Config, Container
from model import User

@route('/user/<userid>')
def user_info(userid):
    response.content_type = 'application/json'
    user_info = User.info(userid)

    return json.dumps(user_info, indent=4)

def main():
    Config.config.init_resources()
    Config.config.from_ini("config.ini")

    db = Container.db()
    db.connect()

    run(host='localhost', port=8888)

    db.disconnect()

if __name__ == '__main__':
    main()